package com.bsa.springdata.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findByLastNameStartsWithIgnoreCaseOrderByLastNameAsc(String lastName, Pageable pageable);

    List<User> findByOfficeCityOrderByLastNameAsc(String city);

    List<User> findByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    List<User> findByOfficeCityAndTeamRoom(String city, String room, Sort ascending);

    int deleteByExperienceLessThan(int experience);
}