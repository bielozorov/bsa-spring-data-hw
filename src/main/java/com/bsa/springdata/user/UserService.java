package com.bsa.springdata.user;

import com.bsa.springdata.office.Office;
import com.bsa.springdata.office.OfficeRepository;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.user.dto.CreateUserDto;
import com.bsa.springdata.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private TeamRepository teamRepository;

    public Optional<UUID> safeCreateUser(CreateUserDto userDto) {
        Optional<Office> office = officeRepository.findById(userDto.getOfficeId());
        Optional<Team> team = teamRepository.findById((userDto.getTeamId()));

        return office.flatMap(o -> team.map(t -> {
            User user = User.fromDto(userDto, o, t);
            User result = userRepository.save(user);
            return result.getId();
        }));
    }

    public Optional<UUID> createUser(CreateUserDto userDto) {
        try {
            Office office = officeRepository.getOne(userDto.getOfficeId());
            Team team = teamRepository.getOne(userDto.getTeamId());
            User user = User.fromDto(userDto, office, team);
            User result = userRepository.save(user);
            return Optional.of(result.getId());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public Optional<UserDto> getUserById(UUID id) {
        return userRepository.findById(id).map(UserDto::fromEntity);
    }

    public List<UserDto> getUsers() {
        return userRepository
                .findAll()
                .stream()
                .map(UserDto::fromEntity)
                .collect(Collectors.toList());
    }

    public List<UserDto> findByLastName(String lastName, int page, int size) {
        // TODO: Use a single query. Use class Sort to sort users by last name. Try to avoid @Query annotation here
        return userRepository
                .findByLastNameStartsWithIgnoreCaseOrderByLastNameAsc(lastName, PageRequest.of(page, size))
                .stream()
                .map(UserDto::fromEntity)
                .collect(Collectors.toList());
    }

    public List<UserDto> findByCity(String city) {
        // TODO: Use a single query. Sort users by last name
        return userRepository
                .findByOfficeCityOrderByLastNameAsc(city)
                .stream()
                .map(UserDto::fromEntity)
                .collect(Collectors.toList());
    }

    public List<UserDto> findByExperience(int experience) {
        // TODO: Use a single query. Sort users by experience by descending. Try to avoid @Query annotation here
        return userRepository
                .findByExperienceGreaterThanEqualOrderByExperienceDesc(experience)
                .stream()
                .map(UserDto::fromEntity)
                .collect(Collectors.toList());
    }

    public List<UserDto> findByRoomAndCity(String city, String room) {
        // TODO: Use a single query. Use class Sort to sort users by last name.
        return userRepository
                .findByOfficeCityAndTeamRoom(city, room, Sort.sort(User.class).by(User::getLastName)
                        .ascending())
                .stream()
                .map(UserDto::fromEntity)
                .collect(Collectors.toList());
    }

    public int deleteByExperience(int experience) {
        // TODO: Use a single query. Return a number of deleted rows
        return userRepository.deleteByExperienceLessThan(experience);
    }
}
